// Fill out your copyright notice in the Description page of Project Settings.

#include "MadTrackerMovementComponent.h"
#include "GameFramework/GameStateBase.h"

static const float FORCE_MULTIPLYER = 100;

// Sets default values for this component's properties
UMadTrackerMovementComponent::UMadTrackerMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UMadTrackerMovementComponent::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void UMadTrackerMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetOwnerRole() == ENetRole::ROLE_AutonomousProxy || GetOwner()->GetRemoteRole() == ENetRole::ROLE_SimulatedProxy)
	{
		Move = CreateMove(DeltaTime);
		SimulateMove(Move);
	}
}

void UMadTrackerMovementComponent::SetVelocity(FVector NewVelocity)
{
	Velocity = NewVelocity;
}

void UMadTrackerMovementComponent::SetThrottle(float NewThrottle)
{
	Throttle = NewThrottle;
}

void UMadTrackerMovementComponent::SetSteeringThrow(float NewSteeringThrow)
{
	SteeringThrow = NewSteeringThrow;
}

void UMadTrackerMovementComponent::SimulateMove(const FMadTrackerPawnMove& InMove)
{
	FVector Force = GetOwner()->GetActorForwardVector() * MaxDrivingForce * InMove.Throttle * FORCE_MULTIPLYER;

	//if (GetOwner()->GetRemoteRole() == ENetRole::ROLE_SimulatedProxy)
	//{
	//	UE_LOG(LogTemp, Warning, TEXT("Throttle: %f, SteeringThrow: %f, Force: %d"), InMove.Throttle, InMove.SteeringThrow, Force.Size());
	//}
	
	Force += CalculateAirResistanceForce();
	Force += CalculateRollingResistanceForce();

	FVector Acceleration = Force / Mass;

	//if (GEngine) 
	//{
	//	int SpeedInKmH = Velocity.Size() * 0.00001f * 3600;
	//	GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Yellow, FString::Printf(TEXT("Speed: %d km/h"), SpeedInKmH));
	//}

	Velocity += Acceleration * Move.DeltaTime;

	UpdateRotationFromInput(Move.DeltaTime, Move.SteeringThrow);
	UpdateLocationFromVelocity(Move.DeltaTime);
}

FMadTrackerPawnMove UMadTrackerMovementComponent::CreateMove(float INDeltaTime)
{
	FMadTrackerPawnMove L_Move;
	L_Move.DeltaTime = INDeltaTime;
	L_Move.SteeringThrow = SteeringThrow;
	L_Move.Throttle = Throttle;
	L_Move.Time = GetWorld()->TimeSeconds;

	return L_Move;
}

FVector UMadTrackerMovementComponent::CalculateAirResistanceForce()
{
	return  -Velocity.GetSafeNormal() * Velocity.SizeSquared() * DragCoefficient;
}

FVector UMadTrackerMovementComponent::CalculateRollingResistanceForce()
{
	FVector RollingResistance = -Velocity.GetSafeNormal() * RollingresistanceCoefficient * -(GetWorld()->GetGravityZ());
	return RollingResistance;
}

void UMadTrackerMovementComponent::UpdateRotationFromInput(float INDeltaTime, float INSteeringThrow)
{
	int DrivingDirection = (FVector::DotProduct(Velocity, GetOwner()->GetActorForwardVector()) < 0 ? -1 : 1);
	auto DeltaLocation = Velocity.Size() * INDeltaTime;

	float RotationAngle = DeltaLocation / MinTurningRadius * INSteeringThrow * DrivingDirection * 0.01;
	FQuat RotationDelta(GetOwner()->GetActorUpVector(), RotationAngle);

	Velocity = RotationDelta.RotateVector(Velocity);

	GetOwner()->AddActorWorldRotation(RotationDelta, true);
}

void UMadTrackerMovementComponent::UpdateLocationFromVelocity(float INDeltaTime)
{
	FVector Translation = Velocity * INDeltaTime;
	FHitResult TranslationCollisionResult;
	GetOwner()->AddActorWorldOffset(Translation, true, &TranslationCollisionResult);
	if (TranslationCollisionResult.IsValidBlockingHit())
	{
		Velocity = FVector::ZeroVector;
	}
}