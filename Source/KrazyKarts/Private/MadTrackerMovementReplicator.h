// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MadTrackerMovementComponent.h"
#include "MadTrackerMovementReplicator.generated.h"

USTRUCT()
struct FMadTrackerState
{
	GENERATED_BODY()

	UPROPERTY()
	FVector Velocity;

	UPROPERTY()
	FMadTrackerPawnMove LastMove;

	UPROPERTY()
	FTransform Transform;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UMadTrackerMovementReplicator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMadTrackerMovementReplicator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void AddUnacknowledgedMove(FMadTrackerPawnMove Move);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SendMove(FMadTrackerPawnMove PawnMovement);

private:
	void ClearAcknowledgedMoves(const FMadTrackerPawnMove& LastPawnMove);

	void UpdateServerState(const FMadTrackerPawnMove& PawnMovement);

	//State of this actor on the server
	UPROPERTY(ReplicatedUsing = OnRep_ServerState)
	FMadTrackerState ServerState;

	UFUNCTION()
	void OnRep_ServerState();

	TArray<FMadTrackerPawnMove> UnacknowledgedMoves;

	UPROPERTY()
	UMadTrackerMovementComponent* MovementComponent;
};
