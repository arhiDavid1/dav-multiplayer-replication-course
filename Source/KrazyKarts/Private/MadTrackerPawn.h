// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MadTrackerMovementComponent.h"
#include "MadTrackerPawn.generated.h"

class UMadTrackerMovementReplicator;

UCLASS()
class AMadTrackerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMadTrackerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void ForwardBackwardMovement(float InputValue);

	UFUNCTION()
	void LeftRightRotation(float InputValue);

private:
	UPROPERTY(VisibleAnywhere)
	UMadTrackerMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnywhere)
	UMadTrackerMovementReplicator* MovementReplicator;
};
