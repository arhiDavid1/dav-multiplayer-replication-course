// Fill out your copyright notice in the Description page of Project Settings.


#include "MadTrackerPawn.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "MadTrackerMovementReplicator.h"

static const FName MOVEMENT_COMPONENT_NAME = "MovementComponent";
static const FName NET_REPLICATOR_NAME = "MovementReplicator";
static const FName FORWARD_BACKWARD_INPUT_NAME = "ForwardBackwardMovement";
static const FName LEFT_RIGHT_INPUT_NAME = "LeftRightTurning";

// Sets default values
AMadTrackerPawn::AMadTrackerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	MovementComponent = CreateDefaultSubobject<UMadTrackerMovementComponent>(MOVEMENT_COMPONENT_NAME);
	MovementReplicator = CreateDefaultSubobject<UMadTrackerMovementReplicator>(NET_REPLICATOR_NAME);
}

// Called when the game starts or when spawned
void AMadTrackerPawn::BeginPlay()
{
	Super::BeginPlay();
	if (HasAuthority())
	{
		NetUpdateFrequency = 1;
	}
}

// Called every frame
void AMadTrackerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMadTrackerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// set up gameplay key bindings
	check(InputComponent);

	InputComponent->BindAxis(FORWARD_BACKWARD_INPUT_NAME, this, &AMadTrackerPawn::ForwardBackwardMovement);
	InputComponent->BindAxis(LEFT_RIGHT_INPUT_NAME, this, &AMadTrackerPawn::LeftRightRotation);
}

void AMadTrackerPawn::ForwardBackwardMovement(float InputValue)
{
	if (!MovementComponent) return;
	MovementComponent->SetThrottle(InputValue);
}

void AMadTrackerPawn::LeftRightRotation(float InputValue)
{
	if (!MovementComponent) return;
	MovementComponent->SetSteeringThrow(InputValue);
}



