// Fill out your copyright notice in the Description page of Project Settings.


#include "MadTrackerMovementReplicator.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UMadTrackerMovementReplicator::UMadTrackerMovementReplicator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicated(true);
}

// Called when the game starts
void UMadTrackerMovementReplicator::BeginPlay()
{
	Super::BeginPlay();
	MovementComponent = GetOwner()->GetComponentByClass<UMadTrackerMovementComponent>();
}


// Called every frame
void UMadTrackerMovementReplicator::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!MovementComponent) return;

	auto LastMove = MovementComponent->GetLastMove();

	if (GetOwnerRole() == ENetRole::ROLE_AutonomousProxy)
	{		
		UnacknowledgedMoves.Add(LastMove);
		Server_SendMove(LastMove);
	}

	if (GetOwner()->GetRemoteRole() == ENetRole::ROLE_SimulatedProxy)
	{
		UpdateServerState(LastMove);
	}

	if (GetOwnerRole() == ENetRole::ROLE_SimulatedProxy)
	{
		MovementComponent->SimulateMove(ServerState.LastMove);
	}

	FString RoleName;
	UEnum::GetValueAsString(GetOwner()->GetLocalRole(), RoleName);
	DrawDebugString(GetWorld(), FVector(0, 0, 100), RoleName, GetOwner(), FColor::White, DeltaTime);
}

void UMadTrackerMovementReplicator::AddUnacknowledgedMove(FMadTrackerPawnMove Move)
{
	UnacknowledgedMoves.Add(Move);
}

void UMadTrackerMovementReplicator::ClearAcknowledgedMoves(const FMadTrackerPawnMove& LastPawnMove)
{
	TArray<FMadTrackerPawnMove> NewUnacknowledgedMoves;
	for (const FMadTrackerPawnMove& Move : UnacknowledgedMoves)
	{
		auto ThisMovesTime = Move.Time;

		if (ThisMovesTime > LastPawnMove.Time)
		{
			NewUnacknowledgedMoves.Add(Move);
		}
	}

	UnacknowledgedMoves = NewUnacknowledgedMoves;
}

void UMadTrackerMovementReplicator::UpdateServerState(const FMadTrackerPawnMove& PawnMovement)
{
	ServerState.LastMove = PawnMovement;
	ServerState.Transform = GetOwner()->GetActorTransform();
	ServerState.Velocity = MovementComponent->GetVelocity();
}

void UMadTrackerMovementReplicator::Server_SendMove_Implementation(FMadTrackerPawnMove PawnMovement)
{
	if (!MovementComponent) return;

	MovementComponent->SimulateMove(PawnMovement);

	UpdateServerState(PawnMovement);
}

bool UMadTrackerMovementReplicator::Server_SendMove_Validate(FMadTrackerPawnMove PawnMovement)
{
	return /*FMath::IsWithinInclusive(InputValue, -1, 1)*/ true; //TODO: implement validation
}

void UMadTrackerMovementReplicator::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UMadTrackerMovementReplicator, ServerState);
}

void UMadTrackerMovementReplicator::OnRep_ServerState()
{
	if (!MovementComponent) return;
	GetOwner()->SetActorTransform(ServerState.Transform);
	MovementComponent->SetVelocity(ServerState.Velocity);
	ClearAcknowledgedMoves(ServerState.LastMove);

	for (const FMadTrackerPawnMove& Move : UnacknowledgedMoves)
	{
		MovementComponent->SimulateMove(Move);
	}
}