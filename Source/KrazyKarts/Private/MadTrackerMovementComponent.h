// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MadTrackerMovementComponent.generated.h"

USTRUCT()
struct FMadTrackerPawnMove
{
	GENERATED_BODY()

	UPROPERTY()
	float DeltaTime;

	UPROPERTY()
	float Time;

	UPROPERTY()
	float Throttle;

	UPROPERTY()
	float SteeringThrow;
};

class UMadTrackerMovementReplicator;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class UMadTrackerMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UMadTrackerMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void SetVelocity(FVector NewVelocity);

	void SetThrottle(float NewThrottle);

	void SetSteeringThrow(float NewSteeringThrow);

	FVector GetVelocity() const { return Velocity; }

	FMadTrackerPawnMove CreateMove(float INDeltaTime);

	UFUNCTION()
	void SimulateMove(const FMadTrackerPawnMove& InMove);

	FMadTrackerPawnMove GetLastMove() const { return Move; };

private:
	void UpdateRotationFromInput(float INDeltaTime, float INSteeringThrow);

	void UpdateLocationFromVelocity(float INDeltaTime);

	UPROPERTY(EditAnywhere)
		float Mass = 1000;

	//Max force applied to the car when the throttle is fully down (N)
	UPROPERTY(EditAnywhere)
		float MaxDrivingForce = 20000;

	//Minimum radius of the car turning circle at full lock (m)
	UPROPERTY(EditAnywhere)
		float MinTurningRadius = 10;

	UPROPERTY(EditAnywhere)
		float RollingresistanceCoefficient = 1;

	//Resistance in kg/m ?
	UPROPERTY(EditAnywhere)
		float DragCoefficient = 1;

	FVector CalculateAirResistanceForce();
	FVector CalculateRollingResistanceForce();

	float Throttle = 0;
	float SteeringThrow = 0;

	FVector Velocity = FVector::ZeroVector;
	FVector DesiredVelocity = FVector::ZeroVector;

	FMadTrackerPawnMove Move;
};
